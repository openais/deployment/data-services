---
# Hugo K/V pairs 
title: Data Services
comments: false
weight: 30
---

<!-- # Data Services -->

This is the Docker Stack that is responsible for storing and transmitting AIS data. This includes
 
  - [Database](https://docs.timescale.com/self-hosted/latest/install/installation-docker/): Postgresql RDMS + PostGIS + TimeScaleDB 
  - [Message Broker](https://rabbitmq.com/): RabbitMQ message broker
  - [OGC API](https://access.crunchydata.com/documentation/pg_featureserv/1.2.0/): PG Featureserv OGC compliant API tool

## Configure

An example configuration file is available at `./config/example.env`. This file must be copied to `.env` and edited to reflect your local environment. This is then used to deploy the docker-compose file via the command line, or preferably, Portainer.

The config variables are:
  - Image `TAG` : These are the image tags associated to the docker images used in the docker-compose file. 
  - Default User/Password credentials: These are the initial credentials that are used to access the services.
  - DB Host details: Ports, DNS names etc. As well as the database to be created.
  - TRAEFIK Hostname: The suffix of the URL to use to reach the services. 

## Deployment

On Portainer navigate to Stacks>Add Stack:
  - Give it a name
  - Select "Repository" as build method
  - Input "https://gitlab.com/openais/deployment/data-services" as the repository URL
  - Add Environment variables, either one by one or from a file that you've already edited to represent your deployment environment.
  - Deploy the stack


If using Docker Compose as the deployment method do the following:
```
git clone git@gitlab.com:openais/deployment/data-services.git
cd ./data-services
cp ./config/example.env .env
<edit the env file>
docker-compose up -d
```
